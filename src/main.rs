//! wgetj command line application.
#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]
#![cfg_attr(feature="clippy", deny(clippy, clippy_pedantic))]
#![deny(missing_docs)]
extern crate docopt;
extern crate libmultilog;
extern crate libwgetj;
#[macro_use]
extern crate log;
extern crate rustc_serialize;

use docopt::Docopt;
use libmultilog::multi::{init_multi_logger, MultiLogger};
use libwgetj::{DownloadConfig, latest};
use libwgetj::Arch::I586;
use libwgetj::Archive::{RPM, TGZ, DMG, EXE};
use libwgetj::Package::JRE;
use libwgetj::OS::{Mac, Windows};
use libwgetj::Version::{Seven, Eight};
use log::{LogLevelFilter, LogRecord};
use std::io::{self, Write};
use std::path::PathBuf;
use std::process;

mod version;

#[cfg_attr(rustfmt, rustfmt_skip)]
/// Usage string.
static USAGE: &'static str = "wgetj - Download Java distributions

Usage:
    wgetj [options]
    wgetj list <opt>
    wgetj (-h | --help)
    wgetj (-V | --version)

Options:
    -d --dir <dir>       Set the download directory.
    -v --jversion <ver>  Set the Java version. [default: 8]
    -a --arch <arch>     Set the architecture. [default: 64]
    -t --type <type>     Set the distribution type. [default: jdk]
    -o --os <os>         Set the target OS. [default: lin]
    -p --pr <pr>         Set the point release. [default: latest]
    --archive <acv>      Set the distribution packaging. [default: tgz]
    --dry-run            Don't actually download the dist.  Print the URL to stdout.
    --verbose            Verbose output.
    -h --help            Show this message.
    -V --version         Show wgetj version.
";

#[derive(RustcDecodable, Debug)]
/// Command Line Arguments
struct Args {
    /// List the valid options for an argument
    arg_opt: String,
    /// List command
    cmd_list: bool,
    /// OS Architecture flag
    flag_arch: u8,
    /// Archive Type
    flag_archive: String,
    /// Output Directory
    flag_dir: String,
    /// Dryrun flag
    flag_dry_run: bool,
    /// Help flag
    flag_help: bool,
    /// Java Version
    flag_jversion: u8,
    /// OS Type
    flag_os: String,
    /// Point Release
    flag_pr: String,
    /// Distribution Type
    flag_type: String,
    /// Verbose Flag
    flag_verbose: bool,
    /// Version Flag
    flag_version: bool,
}

/// Stdout output format.
fn stdoutfn(record: &LogRecord) {
    writeln!(io::stdout(), "{}", record.args()).expect("Unable to write to stdout!");
}

/// Initialize
fn init(verbose: bool) {
    let mut ml: MultiLogger = Default::default();

    ml.enable_stdout(stdoutfn);

    match init_multi_logger(if verbose {
                                LogLevelFilter::Debug
                            } else {
                                LogLevelFilter::Info
                            },
                            ml) {
        Ok(_) => {}
        Err(e) => {
            writeln!(io::stderr(), "Unable to initialize logging! {}", e)
                .expect("Unable to write to stderr!");
        }
    };
}

/// Setup the output path.
fn setup_dir(cfg: &mut DownloadConfig, args: &Args) {
    if !args.flag_dir.is_empty() {
        cfg.dir(Some(PathBuf::from(&args.flag_dir[..])));
    }
}

/// Setup the java version.
fn setup_java_version(cfg: &mut DownloadConfig, args: &Args) {
    match args.flag_jversion {
        7 => {
            cfg.version(Seven);
        }
        8 => {}
        _ => {
            error!("wgetj: Unknown version! Use '7' or '8'");
            process::exit(1);
        }
    }
}

/// Setup the OS architecture.
fn setup_cpu_architecture(cfg: &mut DownloadConfig, args: &Args) {
    match args.flag_arch {
        32 => {
            cfg.arch(I586);
        }
        64 => {}
        _ => {
            error!("wgetj: Unknown cpu architecture! Use '64' or '32'");
            process::exit(1);
        }
    }
}

/// Setup the package type.
fn setup_package(cfg: &mut DownloadConfig, args: &Args) {
    match &args.flag_type[..] {
        "jdk" => {}
        "jre" => {
            cfg.package(JRE);
        }
        _ => {
            error!("wgetj: Unknown type! Use 'jdk' or 'jre'");
            process::exit(1);
        }
    }
}

/// Setup the OS type.
fn setup_os(cfg: &mut DownloadConfig, args: &Args) {
    match &args.flag_os[..] {
        "lin" => {}
        "mac" => {
            cfg.os(Mac);
        }
        "win" => {
            cfg.os(Windows);
        }
        _ => {
            error!("wgetj: Unknown os! Use 'lin','mac', or 'win'");
            process::exit(1);
        }
    }
}

/// Setup the archive type.
fn setup_archive(cfg: &mut DownloadConfig, args: &Args) {
    match &args.flag_archive[..] {
        "rpm" => {
            cfg.archive(RPM);
        }
        "tgz" => {
            cfg.archive(TGZ);
        }
        "dmg" => {
            cfg.archive(DMG);
        }
        "exe" => {
            cfg.archive(EXE);
        }
        _ => {
            error!("wgetj: Unknown archive! Use 'rpm','tgz', 'dmg' or 'exe'");
            process::exit(1);
        }
    }
}

/// Setup the point release.
fn setup_point_release(cfg: &mut DownloadConfig, args: &Args) {
    if args.flag_pr == "latest" {
        cfg.point_release(latest(match args.flag_jversion {
                                     7 => Seven,
                                     8 => Eight,
                                     _ => unreachable!(),
                                 }));
    } else {
        match args.flag_pr.parse::<u8>() {
            Ok(pr) => {
                cfg.point_release(pr);
            }
            Err(e) => {
                error!("wgetj: Invalid point release! Use a number. {}", e);
                process::exit(1);
            }
        }

    }
}

/// Main wgetj function.
pub fn main() {
    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.decode())
        .unwrap_or_else(|e| e.exit());

    if args.flag_version {
        writeln!(io::stdout(), "{}", version::version(true)).expect("Unable to write to stdout!");
    } else if args.cmd_list {
        writeln!(io::stderr(), "E_NOIMPL: This need to be implemented!")
            .expect("Unable to write to stderr!");
        process::exit(0);
    } else {
        init(args.flag_verbose);
        let mut cfg: DownloadConfig = Default::default();

        setup_dir(&mut cfg, &args);
        setup_java_version(&mut cfg, &args);
        setup_cpu_architecture(&mut cfg, &args);
        setup_package(&mut cfg, &args);
        setup_os(&mut cfg, &args);
        setup_archive(&mut cfg, &args);
        setup_point_release(&mut cfg, &args);

        cfg.dry_run(args.flag_dry_run);

        process::exit(match cfg.download() {
                          Ok(o) => o as i32,
                          Err(e) => {
            error!("{}", e);
            1
        }
                      })
    }
}
