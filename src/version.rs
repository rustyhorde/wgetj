//! Version Output
use libwgetj;

include!(concat!(env!("OUT_DIR"), "/version.rs"));

#[cfg(unix)]
/// Generate the verbose version string.
fn verbose_ver() -> String {
    format!("\x1b[32;1mlibwgetj {}\x1b[0m ({} {}) (built {})\ncommit-hash: {}\ncommit-date: \
             {}\nbuild-date: {}\nhost: {}\nrelease: {}\n\n{}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver(),
            libwgetj::version(true))
}

#[cfg(windows)]
/// Generate the verbose version string.
fn verbose_ver() -> String {
    format!("libwgetj {} ({} {}) (built {})\ncommit-hash: {}\ncommit-date: {}\nbuild-date: \
             {}\nhost: {}\nrelease: {}\n\n{}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            sha(),
            commit_date(),
            short_now(),
            target(),
            semver(),
            libwgetj::version(true))
}

#[cfg(unix)]
/// Generate the version string.
fn ver() -> String {
    format!("\x1b[32;1mlibwgetj {}\x1b[0m ({} {}) (built {})\n{}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            libwgetj::version(false))
}

#[cfg(windows)]
/// Generate the version string.
fn ver() -> String {
    format!("libwgetj {}[0m ({} {}) (built {})\n{}",
            semver(),
            short_sha(),
            commit_date(),
            short_now(),
            libwgetj::version(false))
}

/// Generate a verbose or more terse version string.
pub fn version(verbose: bool) -> String {
    if verbose { verbose_ver() } else { ver() }
}
